# frozen_string_literal: true

lib = File.expand_path('lib', __dir__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'manifesto/core/monkeypatches/version'

Gem::Specification.new do |spec|
  spec.name          = 'manifesto-core-monkeypatches'
  spec.licenses      = ['MIT']
  spec.version       = Manifesto::Core::Monkeypatches::VERSION
  spec.authors       = ['John W Higgins']
  spec.email         = ['wishdev@gmail.com']

  spec.summary       = 'Various monkeypatches to core classes.'
  spec.homepage      = "TODO: Put your gem's website or public repo URL here."

  # Prevent pushing this gem to RubyGems.org.
  # To allow pushes either set the "allowed_push_host"
  # To allow pushing to a single host or delete this section to
  # allow pushing to any host.
  if spec.respond_to?(:metadata)
    spec.metadata['allowed_push_host'] = "TODO: Set to 'http://mygemserver.com'"
  else
    raise 'RubyGems 2.0 or newer is required to protect against ' \
          'public gem pushes.'
  end

  spec.bindir        = 'exe'
  spec.executables   = spec.files.grep(%r{^exe/}) { |f| File.basename(f) }
  spec.require_paths = ['lib']
  spec.files         = `git ls-files -z`.split("\x0").reject do |f|
    f.match(%r{^(test|spec|features)/})
  end

  spec.add_development_dependency 'bundler', '~> 1.16'
  spec.add_development_dependency 'minitest', '~> 5.0'
  spec.add_development_dependency 'pry', '~> 0.11'
  spec.add_development_dependency 'rake', '~> 10.0'
  spec.add_development_dependency 'simplecov', '~> 0.16'

  spec.add_runtime_dependency 'facets', '~> 3.1'
end
