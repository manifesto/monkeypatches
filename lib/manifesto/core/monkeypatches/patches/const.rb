# frozen_string_literal: true

#
class Module
  def const_get_if_exists?(const)
    return const_get(const) if const_defined?(const)
  end
end
