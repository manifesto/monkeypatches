# frozen_string_literal: true

#
class Object
  def get_binding # rubocop:disable Naming/AccessorMethodName
    instance_eval { binding }
  end
end
