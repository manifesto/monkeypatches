# frozen_string_literal: true

require 'ostruct'

#
class Hash
  unless method_defined? :deep_symbolize_keys

    # rubocop:disable Style/RescueModifier

    def deep_symbolize_keys
      Hash[map do |key, value|
             [(key.to_sym rescue key) || key,
              case value
              when Hash
                value.deep_symbolize_keys
              else
                value
              end]
           end]
    end
    # rubocop:enable Style/RescueModifier
  end

  unless method_defined? :deep_symbolize_keys!
    def deep_symbolize_keys!
      replace(deep_symbolize_keys)
    end
  end

  unless method_defined? :to_o
    def to_o
      JSON.parse to_json, object_class: OpenStruct
    end
  end
end
