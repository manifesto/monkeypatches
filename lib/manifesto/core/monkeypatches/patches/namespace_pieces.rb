# frozen_string_literal: true

require 'facets/string/snakecase'

#
class Object
  def namespace_pieces(obj = self.class)
    obj.to_s.split('::')
  end

  def parent
    namespace_pieces[0..-2].inject(Object) do |obj, const|
      obj.const_get(const)
    end
  end

  def parents_who_are?(klass)
    np = namespace_pieces
    (np.size - 1).downto(0).map do |idx|
      const_get(np.slice(0..idx).join('::'))
        .yield_self { |const| const.is_a?(klass) ? const : nil }
    end.compact.first
  end

  def short_name
    namespace_pieces.last.snakecase
  end
end

#
class Module < Object
  def namespace_pieces
    super(self)
  end
end
