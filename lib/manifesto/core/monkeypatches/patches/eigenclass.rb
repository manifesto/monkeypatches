# frozen_string_literal: true

#
class Object
  def eigenclass
    class << self
      self
    end
  end
end
