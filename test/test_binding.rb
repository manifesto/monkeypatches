# frozen_string_literal: true

require_relative 'helper'

require 'patches/binding'

class BindingTest < MiniTest::Test
  def test_binding
    assert_equal BindingTest, BindingTest.get_binding.receiver
  end
end
