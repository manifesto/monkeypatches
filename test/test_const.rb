# frozen_string_literal: true

require_relative 'helper'

require 'patches/const'

module Bob
  class A
  end
end

class ConstTest < MiniTest::Test
  def test_const_get_if_exists
    assert_nil Bob.const_get_if_exists?(:Steve)
    assert_equal Bob::A, Bob.const_get_if_exists?(:A)
  end
end
