# frozen_string_literal: true

require_relative 'helper'

require 'patches/namespace_pieces'

module A
  module B
    class C
    end
  end
end

class NamespacePiecesTest < MiniTest::Test
  def test_module
    assert_equal %w[A B], A::B.namespace_pieces
  end

  def test_class
    assert_equal %w[A B C], A::B::C.namespace_pieces
  end
end

class ClassTest < MiniTest::Test
  def test_module
    assert_equal A, A::B.parent
  end

  def test_class
    assert_equal A::B, A::B::C.parent
  end
end
