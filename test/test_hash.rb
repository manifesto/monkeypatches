# frozen_string_literal: true

require_relative 'helper'

require 'patches/hash'

class HashTest < MiniTest::Test
  def setup
    @hash = {
      'a' => 'b',
      'b' => {
        'c' => 'd'
      }
    }
  end

  def test_hash
    assert_equal 'b', @hash.deep_symbolize_keys[:a]
    assert_equal 'd', @hash.deep_symbolize_keys[:b][:c]
    assert_nil @hash[:a]
  end

  def test_hash!
    @hash.deep_symbolize_keys!
    assert_equal 'b', @hash[:a]
    assert_nil @hash['a']
  end
end
